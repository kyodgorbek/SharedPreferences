package com.example.sharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    private static final String PREF_NAME = "last_word";
    private static final String KEY_WORD = "KEY_WORD";

    private sharedPreferences mPrefs;
    private EditText mWord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPrefs = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        mWord = (EditText) findViewById(R.id.etPreferences);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(KEY_WORD, mWord.getText(), toString());
                editor.commit();
                break;
            case R.id.btnShow:
                Toast.makeText(view.getContext(), mPrefs.getString(KEY_WORD, "No Word Yet");
                Toast.LENGTH_LONG).show();
        }
    }
}

